#GOALS
#--------------------------

#Analyse Bionano FILE.VCF with DLE-1


#COMMAND LINE
#--------------------------
#perl ./01_Analyse_vcf_bionano.pl -D File.VCF


#SHELLBANG
#!/usr/local/bin/perl -w

#LIBRARY
use Getopt::Long;
use strict ;
use Carp ;
use Data::Dumper;
use List::Util qw(min max) ;


my $VAR1 ;

GetOptions("DLE|D=s"=>\$VAR1) or die("Error in command line arguments\n");


#INPUT = file.vcf
#Chrom_ok(0)	Pos_ok(1)	ID(2)	ALL_Ref(3)	ALL_alt1,..,ALL_altn(4)	quality(5)	FILTER(6)	INFO(7)	FORMAT(8)	genot(9)
open (VAR1, "$VAR1") || die "Probleme d'ouverture : $!" ;	#fichier txt tabulé
printf "fichier $VAR1 : ouvert\n" ;


#Correspondence file between Bionano cmap names and Chr/utg names in the files reference.fasta
my $refcorresp ;
if ( $VAR1 =~ /.*Col0TAIR10.*/ ) {	$refcorresp = "/path/CorrespBionanoRef_Col0TAIR10.txt" ; }
if ( $VAR1 =~ /.*Ler1Genbank.*/ ) {	$refcorresp = "/path/CorrespBionanoRef_Ler1Genbank.txt" ; }
open (CORRESP, "$refcorresp") || die "Probleme d'ouverture : $!" ;	#fichier txt tabulé
printf "fichier $refcorresp : ouvert\n" ;

my %NAME ;
while (my $line = <CORRESP>) {
	chomp($line) ;
	printf "$line\n" ;
	my @CHR = split ("\t", $line) ;
	#col0=nameBionano	col1=nameFASTA
	#printf "$CHR[0]\n" ;
	#printf "$CHR[1]\n" ;
	$NAME{$CHR[0]}=$CHR[1] ;
	printf "$NAME{$CHR[0]}\n" ;
}
#my $tmp = keys(%NAME) ;

#counters
#$NBpos = NB position totale
my $D_NBpos =  0  ;


#SV information storage
my %VCF1 ;


#SV_DLE-1.vcf browsing and information storage
#---------------------------------------------------
while ( my $line = <VAR1> ) {
	chomp ($line ) ;
	
	#file.vcf header
	if ( $line =~ /##.+/ ) {
		#printf START1 "$line\n" ;
		next ;
	}
	elsif ( $line =~ /#CHR.+/ ) { next ; }
	
	else {
		
		$D_NBpos++ ;
		
		#@var
		#Chrom_ok(0)	Pos_ok(1)	ID(2)	ALL_Ref(3)	ALL_alt1,..,ALL_altn(4)	quality(5)	FILTER(6)	INFO(7)	FORMAT(8)	genot(9)
		my @var = split("\t", $line) ;
		#@info
		#SVTYPE(0);END(1);SVLEN(2);CIPOS(3);CIEND(4);EXPERIMENT(5)
		#or TRAnslocation
		#SVTYPE(0);CHR=chr(1);END(2);SVLEN(3);CIPOS(4);CIEND(5);EXPERIMENT(6)
		my @info = split(";", $var[7]) ;
		#unique key for file VAR1=file DLE-1
		my $cle = "D_".$var[2] ;
		
		#storage of the line to print in the output
		$VCF1{$cle}{"stock"} = $line."\t" ;
		my $chr = $var[0] ;
		$chr =~s/chr// ;
		#storage of Bionano chr to sort before printing in the output
		$VCF1{$cle}{"chr"} = $chr ;
		#stockage start "juste/réel"
		$VCF1{$cle}{"pos"} = $var[1] ;
		
		#quality storage ("." and number)
		my $qual = "$var[5]" ;
		#if ( $qual eq "." ) { $VCF1{$cle}{"qual"} = 0 }
		#else { $VCF1{$cle}{"qual"} = $var[5] ; }
		$VCF1{$cle}{"qual"} = $var[5] ;
		
		#SV type storage
		my $type = $info[0] ;
		$type =~ s/SVTYPE=// ;
		$VCF1{$cle}{"type"} = $type ;
		
		#translocation (TRA) case
		if ( $type eq "TRA" ) {
			
			#SV size storage : for deletion length<0, for insertion length>0
			my $length = $info[3] ;
			$length =~ s/SVLEN=// ;
			$VCF1{$cle}{"len"} = $length ;
		
			#SV approximated start storage start
			my $start = $var[1] ;
			my $cipos = $info[4] ;
			if ( $cipos =~ /.*--.*/ ) { 
				#without approximation
				$VCF1{$cle}{"start"} = $start ;
			}
			else {
				#recovering of the positive value of the approximation
				$cipos =~ s/CIPOS.*,// ;
				$VCF1{$cle}{"start"} = $start-$cipos ;
			}
		
			#SV approximated end storage
			my $end = $info[2] ; ##TAKE CARE to TRA cases
			$end =~ s/END=// ;
			$VCF1{$cle}{"fin"} = $end ;
			my $ciend = $info[4] ;
			if ( $ciend =~ /.*,-.*/ ) {
				#without approximation
				$VCF1{$cle}{"end"} = $end ;
			}
			else {
				#recovering of the positive value of the approximation
				$ciend =~ s/CIEND.*,// ;
				$VCF1{$cle}{"end"} = $end+$ciend ;
			}
		}
		else {
			#SV size storage : for deletion length<0, for insertion length>0
			my $length = $info[2] ;
			$length =~ s/SVLEN=// ;
			$VCF1{$cle}{"len"} = $length ;
		
			#SV approximated start storage
			my $start = $var[1] ;
			my $cipos = $info[3] ;
			if ( $cipos =~ /.*--.*/ ) {
				#without approximation
				$VCF1{$cle}{"start"} = $start ;
			}
			else {
				#recovering of the positive value of the approximation
				$cipos =~ s/CIPOS.*,// ;
				$VCF1{$cle}{"start"} = $start-$cipos ;
			}
		
			#SV approximated end storage
			my $end = $info[1] ;
			$end =~ s/END=// ;
			$VCF1{$cle}{"fin"} = $end ;
			my $ciend = $info[4] ;
			if ( $ciend =~ /.*,-.*/ ) {
				#without approximation
				$VCF1{$cle}{"end"} = $end ; }
			else {
				#recovering of the positive value of the approximation
				$ciend =~ s/CIEND.*,// ;
				$VCF1{$cle}{"end"} = $end+$ciend ;
			}
		}
	}

}


close VAR1 ;


#OUTPUT DLE-1 ##########################################################################################################################
#OUT1 - ALL SV DLE-1 - start-end EXACTS ( .BED for bedtools intersect )
my $OUTPUT1 = $VAR1 ;
$OUTPUT1 =~ s/Col_.*_contre_/Bionano_DLE-1_COLvs/ ;
$OUTPUT1 =~ s/Ler_.*_contre_/Bionano_DLE-1_LERvs/ ;
my $DLEEXACT = $OUTPUT1 ;
$DLEEXACT =~ s/\.vcf/_EXACT\.bed/ ;
open (OUT1, ">$DLEEXACT") || die "Probleme d'ouverture : $!" ;	#fichier txt tabulé
printf "fichier $DLEEXACT : ouvert\nEn-tete :\n#CHROM(chr)\tPOS(pos)\ENDexact(fin)\tTYPE:SIZE:QUAL(type:len:qual)\n" ;
#OUT2 - SV (INS,DEL,DUP,INV) DLE-1 - start-end APPROX ( .BED for bedtools intersect )
my $DLE = $OUTPUT1 ;
$DLE =~ s/\.vcf/\.bed/ ;
open (OUT2, ">$DLE") || die "Probleme d'ouverture : $!" ;	#fichier txt tabulé
printf "fichier $DLE : ouvert\nEn-tete :\n#CHROM(chr)\tSTART(start)\END(end)\tTYPE:SIZE:QUAL(type:len:qual)\n" ;
#OUT3 - SV (organelle,INVDUP,TRA) DLE-1 - start-end APPROX ( .BED for bedtools intersect )
my $DLEmanual = $OUTPUT1 ;
$DLEmanual =~ s/\.vcf/_manual\.bed/ ;
open (OUT3, ">$DLEmanual") || die "Probleme d'ouverture : $!" ;	#fichier txt tabulé
printf "fichier $DLEmanual : ouvert\nEn-tete :\n#CHROM(chr)\tSTART(start)\END(end)\tTYPE:SIZE:QUAL(type:len:qual)\n" ;

#OUT4 - SV (INS,DEL,DUP,INV) DLE-1 - start-end APPROX  avec qual >=20 ( .BED for bedtools intersect )
my $DLEqual = $OUTPUT1 ;
$DLEqual =~ s/\.vcf/_qual\.bed/ ;
open (OUT4, ">$DLEqual") || die "Probleme d'ouverture : $!" ;	#fichier txt tabulé
printf "fichier $DLEqual : ouvert\nEn-tete :\n#CHROM(chr)\tSTART(start)\END(end)\tTYPE:SIZE:QUAL(type:len:qual)\n" ;
#OUT5 - SV (INS,DEL,DUP,INV) DLE-1 - start-end APPROX  avec length > 1000 ( .BED for bedtools intersect )
my $DLElen = $OUTPUT1 ;
$DLElen =~ s/\.vcf/_length\.bed/ ;
open (OUT5, ">$DLElen") || die "Probleme d'ouverture : $!" ;	#fichier txt tabulé
printf "fichier $DLElen : ouvert\nEn-tete :\n#CHROM(chr)\tSTART(start)\END(end)\tTYPE:SIZE:QUAL(type:len:qual)\n" ;
#OUT6 - SV (INS,DEL,DUP,INV) DLE-1 - start-end APPROX  avec qual >=20 et length > 1000 ( .BED for bedtools intersect )
my $DLEqualen = $OUTPUT1 ;
$DLEqualen =~ s/\.vcf/_qual_length\.bed/ ;
open (OUT6, ">$DLEqualen") || die "Probleme d'ouverture : $!" ;	#fichier txt tabulé
printf "fichier $DLEqualen : ouvert\nEn-tete :\n#CHROM(chr)\tSTART(start)\END(end)\tTYPE:SIZE:QUAL(type:len:qual)\n" ;


#To count DLE-1 SV
#total
my ( $D_NBsv , $D_INS , $D_DEL , $D_DUP , $D_INV , $D_INVDUP , $D_TRA ) = ( 0 , 0 , 0 , 0 , 0 , 0 , 0 ) ;
#manual
my ( $D_NBman , $D_manINS , $D_manDEL , $D_manDUP , $D_manINV , $D_manINVDUP , $D_manTRA ) = ( 0 , 0 , 0 , 0 , 0 , 0 , 0 ) ;
#qual>=20
my ( $D_NBqual , $D_qualINS , $D_qualDEL , $D_qualDUP , $D_qualINV , $D_qualINVDUP , $D_qualTRA ) = ( 0 , 0 , 0 , 0 , 0 , 0 , 0 ) ;
#len>1000
my ( $D_NBlen , $D_lenINS , $D_lenDEL , $D_lenDUP , $D_lenINV , $D_lenINVDUP , $D_lenTRA ) = ( 0 , 0 , 0 , 0 , 0 , 0 , 0 ) ;
#qual>=20 and len>1000
my ( $D_NBquallen , $D_quallenINS , $D_quallenDEL , $D_quallenDUP , $D_quallenINV , $D_quallenINVDUP , $D_quallenTRA ) = ( 0 , 0 , 0 , 0 , 0 , 0 , 0 ) ;

my $LOG = "AnalyseAuto_VCFbionano_AA-MM-JJ.txt" ;
#my $LOG = "AnalyseAuto_VCFbionano_19-10-09.txt" ;
open (LOG, ">>$LOG") || die "Probleme d'ouverture : $!" ;	#fichier txt tabulé
printf "fichier $LOG : ouvert\n" ;

foreach my $cle ( sort {
	$VCF1{$a}{"chr"} <=> $VCF1{$b}{"chr"}
	||
	$VCF1{$a}{"pos"} <=> $VCF1{$b}{"pos"}
	} keys %VCF1) {
	
	my $tmp = "Chr".$VCF1{$cle}{"chr"} ;
	#printf "$tmp\n" ;
			
	if ( $VCF1{$cle}{"type"} eq "TRA" or $VCF1{$cle}{"type"} eq "INVDUP" or $VCF1{$cle}{"chr"} eq "M" or $VCF1{$cle}{"chr"} eq "C" ) {
		#print OUT3 "DLE-1\tscaffold$VCF1{$cle}{\"stock\"}\n" ;
		#for translocation start>end
		if ( $VCF1{$cle}{"type"} eq "TRA" ) { printf OUT3 "$NAME{$tmp}\t$VCF1{$cle}{\"end\"}\t$VCF1{$cle}{\"start\"}\t$VCF1{$cle}{\"type\"}:$VCF1{$cle}{\"len\"}:$VCF1{$cle}{\"qual\"}\n" ; }
		else { printf OUT3 "$NAME{$tmp}\t$VCF1{$cle}{\"start\"}\t$VCF1{$cle}{\"end\"}\t$VCF1{$cle}{\"type\"}:$VCF1{$cle}{\"len\"}:$VCF1{$cle}{\"qual\"}\n" ; }
	}
	else {
		#print output.bed - DLE-1 exact
		#To print fasta file in output.bed
		printf OUT1 "$NAME{$tmp}\t$VCF1{$cle}{\"pos\"}\t$VCF1{$cle}{\"fin\"}\t$VCF1{$cle}{\"type\"}:$VCF1{$cle}{\"len\"}:$VCF1{$cle}{\"qual\"}\n" ;
		#printf "$NAME{$tmp}\n" ;
		#print output.bed - DLE-1 approx
		printf OUT2 "$NAME{$tmp}\t$VCF1{$cle}{\"start\"}\t$VCF1{$cle}{\"end\"}\t$VCF1{$cle}{\"type\"}:$VCF1{$cle}{\"len\"}:$VCF1{$cle}{\"qual\"}\n" ;
		#filtered file on quality (OUT4), SV length (OUT5) and quality&SVlength (OUT6)
		if ( $VCF1{$cle}{"qual"} >= 20 ) { printf OUT4 "$NAME{$tmp}\t$VCF1{$cle}{\"start\"}\t$VCF1{$cle}{\"end\"}\t$VCF1{$cle}{\"type\"}:$VCF1{$cle}{\"len\"}:$VCF1{$cle}{\"qual\"}\n" ;	}
		if ( abs($VCF1{$cle}{"len"}) > 1000 ) { printf OUT5 "$NAME{$tmp}\t$VCF1{$cle}{\"start\"}\t$VCF1{$cle}{\"end\"}\t$VCF1{$cle}{\"type\"}:$VCF1{$cle}{\"len\"}:$VCF1{$cle}{\"qual\"}\n" ;	}
		if ( $VCF1{$cle}{"qual"} >= 20 and abs($VCF1{$cle}{"len"}) > 1000 ) { printf OUT6 "$NAME{$tmp}\t$VCF1{$cle}{\"start\"}\t$VCF1{$cle}{\"end\"}\t$VCF1{$cle}{\"type\"}:$VCF1{$cle}{\"len\"}:$VCF1{$cle}{\"qual\"}\n" ;	}
	}

	
		
	#counting SV per chr and pos (ascending order)
		
	if( $VCF1{$cle}{"type"} eq "TRA" or $VCF1{$cle}{"type"} eq "INVDUP" or $VCF1{$cle}{"chr"} eq "M" or $VCF1{$cle}{"chr"} eq "C" ) {
		#manual count 
		$D_NBman++ ;
		if ( $VCF1{$cle}{"type"} eq "INS") { $D_manINS++ ; }
		if ( $VCF1{$cle}{"type"} eq "DEL") { $D_manDEL++ ; }
		if ( $VCF1{$cle}{"type"} eq "DUP") { $D_manDUP++ ; }
		if ( $VCF1{$cle}{"type"} eq "INV") { $D_manINV++ ; }
		if ( $VCF1{$cle}{"type"} eq "INVDUP") { $D_manINVDUP++ ; }
		if ( $VCF1{$cle}{"type"} eq "TRA") { $D_manTRA++ ; }
	}
	else {
		$D_NBsv++ ;
		if ( $VCF1{$cle}{"type"} eq "INS") { $D_INS++ ; }
		if ( $VCF1{$cle}{"type"} eq "DEL") { $D_DEL++ ; }
		if ( $VCF1{$cle}{"type"} eq "DUP") { $D_DUP++ ; }
		if ( $VCF1{$cle}{"type"} eq "INV") { $D_INV++ ; }
		if ( $VCF1{$cle}{"type"} eq "INVDUP") { $D_INVDUP++ ; }
		if ( $VCF1{$cle}{"type"} eq "TRA") { $D_TRA++ ; }
		
		if ( $VCF1{$cle}{"qual"} >= 20 ) {
			$D_NBqual++ ;
			#printf "$VCF1{$cle}{\"stock\"}\t$VCF1{$cle}{\"type\"}\t$VCF1{$cle}{\"len\"}\t$VCF1{$cle}{\"start\"}\t$VCF1{$cle}{\"end\"}\n" ;
			#printf "$cle\t$VCF1{$cle}{\"qual\"}\t$VCF1{$cle}{\"type\"}\t$VCF1{$cle}{\"len\"}\t$VCF1{$cle}{\"start\"}\t$VCF1{$cle}{\"end\"}\n" ;
			if ( $VCF1{$cle}{"type"} eq "INS") { $D_qualINS++ ; }
			if ( $VCF1{$cle}{"type"} eq "DEL") { $D_qualDEL++ ; }
			if ( $VCF1{$cle}{"type"} eq "DUP") { $D_qualDUP++ ; }
			if ( $VCF1{$cle}{"type"} eq "INV") { $D_qualINV++ ; }
			if ( $VCF1{$cle}{"type"} eq "INVDUP") { $D_qualINVDUP++ ; }
			if ( $VCF1{$cle}{"type"} eq "TRA") { $D_qualTRA++ ; }
		}
		if ( abs($VCF1{$cle}{"len"}) > 1000 ) {
			$D_NBlen++ ;
			#printf "$VCF1{$cle}{\"stock\"}\t$VCF1{$cle}{\"type\"}\t$VCF1{$cle}{\"len\"}\t$VCF1{$cle}{\"start\"}\t$VCF1{$cle}{\"end\"}\n" ;
			#printf "$cle\t$VCF1{$cle}{\"qual\"}\t$VCF1{$cle}{\"type\"}\t$VCF1{$cle}{\"len\"}\t$VCF1{$cle}{\"start\"}\t$VCF1{$cle}{\"end\"}\n" ;
			if ( $VCF1{$cle}{"type"} eq "INS") { $D_lenINS++ ; }
			if ( $VCF1{$cle}{"type"} eq "DEL") { $D_lenDEL++ ; }
			if ( $VCF1{$cle}{"type"} eq "DUP") { $D_lenDUP++ ; }
			if ( $VCF1{$cle}{"type"} eq "INV") { $D_lenINV++ ; }
			if ( $VCF1{$cle}{"type"} eq "INVDUP") { $D_lenINVDUP++ ; }
		}
		if ( $VCF1{$cle}{"qual"} >= 20 and $VCF1{$cle}{"len"} > 1000 ) {
			$D_NBquallen++ ;
			#printf "$VCF1{$cle}{\"stock\"}\t$VCF1{$cle}{\"type\"}\t$VCF1{$cle}{\"len\"}\t$VCF1{$cle}{\"start\"}\t$VCF1{$cle}{\"end\"}\n" ;
			#printf "$cle\t$VCF1{$cle}{\"qual\"}\t$VCF1{$cle}{\"type\"}\t$VCF1{$cle}{\"len\"}\t$VCF1{$cle}{\"start\"}\t$VCF1{$cle}{\"end\"}\n" ;
			if ( $VCF1{$cle}{"type"} eq "INS") { $D_quallenINS++ ; }
			if ( $VCF1{$cle}{"type"} eq "DEL") { $D_quallenDEL++ ; }
			if ( $VCF1{$cle}{"type"} eq "DUP") { $D_quallenDUP++ ; }
			if ( $VCF1{$cle}{"type"} eq "INV") { $D_quallenINV++ ; }
			if ( $VCF1{$cle}{"type"} eq "INVDUP") { $D_quallenINVDUP++ ; }
			if ( $VCF1{$cle}{"type"} eq "TRA") { $D_quallenTRA++ ; }
		}
	}
	

}
	
printf LOG "$VAR1 ($D_NBpos SV)\n" ;
printf LOG "FILTER\tNBsvFILTERED\tNBsvINS\tNBsvDEL\tNBsvDUP\tNBsvINV\tNBsvINVDUP\tNBsvTRA\n" ;
printf LOG "NO\t$D_NBsv\t$D_INS\t$D_DEL\t$D_DUP\t$D_INV\t$D_INVDUP\t$D_TRA\n" ;
printf LOG "Manual\t$D_NBman\t$D_manINS\t$D_manDEL\t$D_manDUP\t$D_manINV\t$D_manINVDUP\t$D_manTRA\n" ;
printf LOG "QUAL>=20\t$D_NBqual\t$D_qualINS\t$D_qualDEL\t$D_qualDUP\t$D_qualINV\t$D_qualINVDUP\t$D_qualTRA\n" ;
printf LOG "LENGTH>1000\t$D_NBlen\t$D_lenINS\t$D_lenDEL\t$D_lenDUP\t$D_lenINV\t$D_lenINVDUP\t$D_lenTRA\n" ;
printf LOG "QUAL>=20&LENGTH>1000\t$D_NBquallen\t$D_quallenINS\t$D_quallenDEL\t$D_quallenDUP\t$D_quallenINV\t$D_quallenINVDUP\t$D_quallenTRA\n" ;



close LOG ;
close OUT1 ;
close OUT2 ;
close OUT3 ;
close OUT4 ;
close OUT5 ;
close OUT6 ;
