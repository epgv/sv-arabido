#GOALS
#--------------------------

#Analysis of MUMmer show-diff output (.diff)


#COMMAND LINE 
#--------------------------
#perl ./01_Analyse_diff_ont.pl


#SHELLBANG
#!/usr/local/bin/perl -w

#LIBRARY
use Getopt::Long;
use strict ;
use Carp ;
use Data::Dumper;
use List::Util qw(min max) ;

#ONT SV filter on SV size
my $filter = 1000 ;

############################### INPUT ###############################################
#####################################################################################

my $path = "/path/directory" ;
my $VAR1 = $path."/file1.diff" ;
#Ler_ONTsmartdenovo vs Ler 2019 - zapata options
my $VAR2 = $path."/file2.diff" ;
my @LISTFILE = ("$VAR1","$VAR2") ;
#####################################################

#To keep in mind
#GAP (gap between two mutually consistent alignments)
#SEQref(0)	TYPE(1)	S1(2)	E1(3)	LENGTH1(4)	LENGTH2(5)	DIFF(ref-query)(6)
#DUP (inserted duplication)
#SEQref(0)	TYPE(1)	S1(2)	E1(3)	LENGTH1(4)
#BREAK (other inserted sequence)
#SEQref(0)	TYPE(1)	S1(2)	E1(3)	LENGTH1(4)
#JMP (rearrangement)
#SEQref(0)	TYPE(1)	S1(2)	E1(3)	LENGTH1(4)
#INV - LENGTH <0
#SEQref(0)	TYPE(1)	S1(2)	E1(3)	LENGTH1(4)
#SEQ (rearrangement with another sequence)
#SEQref(0)	TYPE(1)	S1(2)	E1(3)	LENGTH1(4)	prev-sequence(5) next-sequence(6)


#####################################################################################
#####################################################################################


########################### OUTPUT - LOG ############################################
#####################################################################################

my $LOG = "/output_path/log.csv" ;
open (LOG, ">$LOG") || die "Probleme d'ouverture : $!" ;	#fichier txt tabulé

#####################################################################################
#####################################################################################


foreach my $file ( @LISTFILE ) {

	#$NBpos = NB position totale
	my ( $NBpos , $NBgap , $NBdup, $NBbrk , $NBjmp , $NBinv , $NBseq ) = ( 0 , 0 , 0 , 0 , 0 , 0 , 0 ) ;
	my ( $Fpos , $Fgap , $Fdup, $Fbrk , $Fjmp , $Finv , $Fseq ) = ( 0 , 0 , 0 , 0 , 0 , 0 , 0 ) ;
	
	open (VAR, "$file") || die "Probleme d'ouverture : $!" ;	# Tab file
	printf "fichier $file : ouvert\n" ;

	############################## OUTPUT ###############################################
	#####################################################################################
	my $OUT = $file ;
	$OUT =~ s/\.diff/\.bed/ ;
	#$OUT =~ s/smartDNcons_ALL/ONT_/ ;
	$OUT =~ s/smartDNcons_/ONT_/ ;
	my $OUTman = $OUT ;
	$OUTman =~ s/\.bed/_manual\.bed/ ;
	my $OUTfilter = $OUT ;
	$OUTfilter =~ s/\.bed/_filter\.bed/ ;
	
	open (OUT, ">$OUT") || die "Probleme d'ouverture : $!" ;
	open (OUT2, ">$OUTman") || die "Probleme d'ouverture : $!" ;
	open (OUT3, ">$OUTfilter") || die "Probleme d'ouverture : $!" ;
	
	print OUT "#GAP (gap between two mutually consistent alignments) - SIZE=ref-query\n" ;
	print OUT "#DUP (inserted duplication)\n" ;
	print OUT2 "#GAP (gap between two mutually consistent alignments) - SIZE=ref-query\n" ;
	print OUT2 "#DUP (inserted duplication)\n" ;
	print OUT3 "#GAP (gap between two mutually consistent alignments) - SIZE=ref-query\n" ;
	print OUT3 "#DUP (inserted duplication)\n" ;
	print OUT2 "#BREAK (other inserted sequence)\n" ;
	print OUT2 "#JMP (rearrangement)\n" ;
	print OUT "#INV (rearrangement with inversion) - SIZE <0\n" ;
	print OUT2 "#INV (rearrangement with inversion) - SIZE <0\n" ;
	print OUT3 "#INV (rearrangement with inversion) - SIZE <0\n" ;
	print OUT2 "#SEQ (rearrangement with another sequence) - on dirait le trou entre 2 contigs alignés sur la ref\n" ;
	#printf "fichier $OUT : ouvert\nEn-tete :\n#CHROM\tSTART\tEND\tCOL_LERutg:TYPE:SIZE\n" ;

	#####################################################################################
	#####################################################################################

	

	#initialisation $utg = Query Chromosome (smartDN or public reference)
	my $utg ;
	if ( $file =~ /.*_ALLCOLvs.*/ or $file =~ /.*_ALLcol_vs_C.*/ ) { $utg = "COLutg16" ; }
	if ( $file =~ /.*_ALLcol_vs_L.*/ ) { $utg = "COLutg16" ; }
	if ( $file =~ /.*_ALLLERvs.*/ ) { $utg = "LERutg3" ; }
	if ( $file =~ /.*_FAH67239vs.*/ ) { $utg = "LERutg450" ; }
	if ( $file =~ /.*_FAH68283vs.*/ ) { $utg = "COLutg22" ; }
	if ( $file =~ /.*_COL_10vs.*/ ) { $utg = "COLutg105" ; }
	if ( $file =~ /.*_COL_15vs.*/ ) { $utg = "COLutg14" ; }
	if ( $file =~ /.*_COL_20vs.*/ ) { $utg = "COLutg7" ; }
	if ( $file =~ /.*_LER_10vs.*/ ) { $utg = "LERutg207" ; }
	if ( $file =~ /.*_LER_15vs.*/ ) { $utg = "LERutg10" ; }
	if ( $file =~ /.*_LER_20vs.*/ ) { $utg = "LERutg3" ; }

	

	#BROWSE VAR.diff and information STORAGE
	#---------------------------------------------------
	while ( my $line = <VAR> ) {
		chomp ($line ) ;
	
		#header of VAR.diff
		if ( $line =~ /^Chr.*/ ) {
			$NBpos++ ;
			#@var - colonnes : voir lignes 35-48
			my @var = split("\t", $line) ;
			
			#subject chromosome specification
			my $chr = $var[0] ;
			# if ( $chr eq "ChrM" ) { $chr = "scaffold6" ; }
			# elsif ( $chr eq "ChrC" ) { $chr = "scaffold7" ; }
			# else { $chr =~ s/Chr0/scaffold/ ; }
			if ( $file =~ /.*vsCol0TAIR10\.diff/ ) { $chr =~ s/Chr/Col0TChr/ ; }
			if ( $file =~ /.*vsLer1GenBk\.diff/ ) { $chr =~ s/Chr/Ler1GChr/ ; }
			
			#query chromosome specification and SV counting
			my $comm ;
			
			if ( $var[1] eq "SEQ" ) {
				$NBseq++ ;
				if ( $file =~ /.*_ALLcol_.*/ or $file =~ /.*_FAH68283.*/ or $file =~ /.*_COL_.*/ ) {
					$utg = "COL".$var[6] ;
					$comm = $utg.":".$var[1].":".$var[4] ;
				}
				if ( $file =~ /.*_ALLler_.*/ or $file =~ /.*_FAH67239.*/ or $file =~ /.*_LER_.*/ ) {
					$utg = "LER".$var[6] ;
					$comm = $utg.":".$var[1].":".$var[4] ;
				}
								
				if ( abs($var[4]) > $filter ) { $Fseq++ ; $Fpos++ ; }
	
			}
			elsif ( $var[1] eq "GAP" ) {
				$NBgap++ ;
				$comm = $utg.":".$var[1].":".$var[6] ;
				if ( abs($var[6]) > $filter ) { $Fgap++ ; $Fpos++ ; }
			}
			else {
				$comm = $utg.":".$var[1].":".$var[4] ;
				if ( $var[1] eq "DUP" ) { $NBdup++ ; }
				if ( $var[1] eq "BRK" ) { $NBbrk++ ; }
				if ( $var[1] eq "JMP" ) { $NBjmp++ ; }
				if ( $var[1] eq "INV" ) { $NBinv++ ; }
				if ( abs($var[4]) > $filter ) {
					$Fpos++ ;
					if ( $var[1] eq "DUP" ) { $Fdup++ ; }
					if ( $var[1] eq "BRK" ) { $Fbrk++ ; }
					if ( $var[1] eq "JMP" ) { $Fjmp++ ; }
					if ( $var[1] eq "INV" ) { $Finv++ ; }
				}
			}
		
			#print in specific output files
			if ( $var[2] < $var[3] ) { 
				#printf "var[0]=$var[0]\n" ;
				#if ($var[1] eq "SEQ" ) { printf "line=$line\n"; }
				if ( $var[0] eq "ChrM" or $var[0] eq "ChrC" ) { printf OUT2 "$chr\t$var[2]\t$var[3]\t$comm\n" ;}
				else {
					if ( $var[1] eq "GAP" or $var[1] eq "DUP" or $var[1] eq "INV" ) { 
						printf OUT "$chr\t$var[2]\t$var[3]\t$comm\n" ;
						if ( $var[1] eq "GAP" and abs($var[6]) > $filter ) { printf OUT3 "$chr\t$var[2]\t$var[3]\t$comm\n" ; }
						if ( ( $var[1] eq "DUP" or $var[1] eq "INV" ) and abs($var[4]) > $filter ) { printf OUT3 "$chr\t$var[2]\t$var[3]\t$comm\n" ; }
					}
					if ( $var[1] eq "SEQ" or $var[1] eq "BRK" or $var[1] eq "JMP" ) { printf OUT2 "$chr\t$var[2]\t$var[3]\t$comm\n" ; }
				}
			}
			else {
				if ( $comm =~ /COL.*/ ) { $comm =~ s/COL/col/ ; }
				if ( $comm =~ /LER.*/ ) { $comm =~ s/LER/ler/ ; }
				if ( $var[0] eq "ChrM" or $var[0] eq "ChrC" ) { printf OUT2 "$chr\t$var[3]\t$var[2]\t$comm\n" ;}
				else {
					if ( $var[1] eq "GAP" or $var[1] eq "DUP" or $var[1] eq "INV" ) {
						printf OUT "$chr\t$var[3]\t$var[2]\t$comm\n" ;
						if ( $var[1] eq "GAP" and abs($var[6]) > $filter ) { printf OUT3 "$chr\t$var[3]\t$var[2]\t$comm\n" ; }
						if ( ( $var[1] eq "DUP" or $var[1] eq "INV" ) and abs($var[4]) > $filter ) { printf OUT3 "$chr\t$var[3]\t$var[2]\t$comm\n" ; }
					}
					if ( $var[1] eq "SEQ" or $var[1] eq "BRK" or $var[1] eq "JMP" ) { printf OUT2 "$chr\t$var[3]\t$var[2]\t$comm\n" ; }
				}
			}
		}
		else {
			next ;
		}
	}

	my $filetmp = $file ;
	$filetmp =~ s/.*smartDNcons_// ;
	my $tmp = $NBpos - $NBseq ;
	my $Ftmp = $Fpos - $Fseq ;
	
	#printf LOG "Description ONT SV\n" ;
	printf LOG "InputFile\tfilter\tNBpos\tNBsv\tNBgap\tNBdup\tNBinv\tNBseq\tNBbrk\tNBjmp\n" ;
	printf LOG "$filetmp\tno\t$NBpos\t$tmp\t$NBgap\t$NBdup\t$NBinv\t$NBseq\t$NBbrk\t$NBjmp\n" ;
	printf LOG "$filetmp\t>$filter bp\t$Fpos\t$Ftmp\t$Fgap\t$Fdup\t$Finv\t$Fseq\t$Fbrk\t$Fjmp\n" ;

	close OUT3 ;
	close OUT2 ;
	close OUT ;
	close VAR ;
}

close LOG ;